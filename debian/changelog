php-algo26-idna-convert (4.0.2-3) experimental; urgency=medium

  * Revert "Force system dependencies loading"
  * Simplify build
  * Update Standards-Version to 4.7.1
  * Modernize PHPUnit syntax

 -- David Prévot <taffit@debian.org>  Wed, 26 Feb 2025 13:28:16 +0100

php-algo26-idna-convert (4.0.2-2) experimental; urgency=medium

  * Force system dependencies loading
  * Expect that package is present for debci

 -- David Prévot <taffit@debian.org>  Mon, 04 Mar 2024 22:04:01 +0100

php-algo26-idna-convert (4.0.2-1) experimental; urgency=medium

  [ Matthias Sommerfeld ]
  * Catch invalid Punycode strings (#44)

 -- David Prévot <taffit@debian.org>  Thu, 15 Feb 2024 22:18:20 +0100

php-algo26-idna-convert (4.0.1-1) experimental; urgency=medium

  * Upload new major version to experimental

  [ Matthias Sommerfeld ]
  * fix endless loop with expanded ASCII codepoints (#36)
  * Version 4.0 (#37)

  [ Makar Mikhalchenko ]
  * Fixed issue #41 with mixed up parameters in str_contains() function
    in ToIdn.php (#42)

  [ David Prévot ]
  * Mark package as Multi-Arch: foreign
  * Fix CI
  * Revert "Track version 3 for now"

 -- David Prévot <taffit@debian.org>  Sat, 16 Sep 2023 17:38:44 +0530

php-algo26-idna-convert (3.1.0-1) unstable; urgency=medium

  [ Matthias Sommerfeld ]
  * fix endless loop with expanded ASCII codepoints

 -- David Prévot <taffit@debian.org>  Sat, 27 May 2023 09:39:47 +0200

php-algo26-idna-convert (3.0.5-2) unstable; urgency=medium

  * Upload to unstable
  * Track version 3 for now
  * Provide overrides for pkg-php-tools

 -- David Prévot <taffit@debian.org>  Thu, 25 May 2023 14:39:01 +0200

php-algo26-idna-convert (3.0.5-1) experimental; urgency=low

  * Initial release (New spip dependency)

 -- David Prévot <taffit@debian.org>  Thu, 25 May 2023 11:48:17 +0200
